<?php

namespace App\Http\Controllers;

use Mail;
use App\User;
use Illuminate\Http\Request;
use App\Http\Models\dwr;
use App\Http\Models\dwr_files;
use App\Http\Models\uploads;
use App\Http\Controllers\Controller;


class dwrController extends Controller
{
    /**
     * Create the form for DWR based on input.
     * @return Response
     */
    
    protected $error;

    public function __construct()
    {
        $this->error        = \Session::get('error');
        $this->fileCount    = \Session::get('fileCount');
        $this->folderSize   = \Session::get('folderSize');
        if(\Session::get('uploadId')>0)
            $this->uploadId = \Session::get('uploadId');
        else 
            $this->uploadId = $this->getUploadId(); 

        $this->sub = array_shift((explode(".", $_SERVER['HTTP_HOST'])));
        if(!in_array($this->sub, array_keys(config('companies.companies'))))
            //abort(404, 'Page nout found.');
            $this->sub = 'dwr';
        $this->sub = config("companies.companies.".$this->sub);
    }


    public function getDwrForm()

    {   
        //if(strpos(\Request::ip(),"108.181.50.175") !== false ) phpinfo();
        // dd(\Request::ip());
        
        //$this->sub = config("companies.companies.".$this->sub);
        //dd($this->sub['logo']);

        return view('dwrForm')
            ->withError($this->error)
            ->withCount($this->fileCount)
            ->withSize($this->folderSize)
            ->withUpload($this->uploadId)
            ->withCompany($this->sub);


    }
    


    public function postDwrForm(Request $request)
    {   
        
        //Accessing Session data
        $upload_id=$request->upload_id;
        \Session::set('error', true);
        $folderName= uploads::where('upload_id', $upload_id)->pluck('folder_name');
    
        //Setting Variables
        $folderSize =0;
        $filecount  =0;
        $filetypes  =[];

        //getting uploaded files stats
        if(is_dir($folderName)){$filecount=count(glob($folderName."/*"));}
        else{
            //get upload temp file if not existing make one
            $folderName= uploads::where('upload_id', $upload_id)->pluck('folder_name');
            
            if(strlen($folderName)==0){
                $folderName=$this->sessionDir($upload_id);    
            }
            if(!is_dir($folderName)&&!is_file($folderName)){mkdir($folderName);}

        }

        //getting file stats
        if( $filecount > 0 ){
            $folderSize =$this->getSize($folderName);
            $filetypes  = $this->countfileTypes($folderName);
        }


        //Session update
        \Session::flash('fileCount', $filecount);
        \Session::flash('previousUploadId', $upload_id);
        \Session::flash('folderSize', $folderSize);
        \Session::flash('uploadId', $upload_id);

        //Input Validation
        $rules= array(
            'project_site'          => 'required',
            'project_name'          => 'required',
            'completion_date'       => 'required',
            'contact_name'          => 'required',
            'contact_last_name'     => 'required',
            'contact_title'         => 'required',
            'contact_email'         => 'required|email',
            'alt_contact_email'     => 'email',
            'contact_phone'         => 'required',
            'job_instructions'      => 'required',
            'native_drawing'        => 'required',
            'priority'              => 'required',
            'hourly'                => 'required'
            );

     
        $this->validate($request, $rules);
  
        //Saving to Database
        $dwr    = new dwr;
        $dwr->company_id = $this->sub['id'];
        $dwr->fill($request->input())->save();
        date_default_timezone_set('America/Denver');
        //$data   = array_add(($request->input()), 'date', date('Y-m-d H:i:s'));
        $data   = array_merge($request->input(), array('date' => date('Y-m-d H:i:s'), 'company' => $this->sub));
        $view   = "pdf.dwrpdf";
        $pdf    = \PDF::loadView($view, $data);
        //return $pdf->stream();

        //getting location to save files
        $uploadsDir= $this->uploadDir();

        //save pdf
        $project_name = trim(\Input::get('project_name'));
        $project_name = strtr(
            trim($project_name),
            '`!@#$%^&*()-_=+[]{}<>,.?/|:;\\\'"',
            '                               '
        );

        $project_name = str_replace(' ', '_', $project_name);
       
        $filename = $folderName."/".$project_name.'_dwr.pdf';
        $pdf->save($filename);
        
        
        
        $zipname= $uploadsDir.'.zip';
        //create Zip file 
        $files = $folderName.'/';
        \Zipper::make($zipname)->add($files)->close();

        
        
        $uploadLink=substr($uploadsDir, 11);
        //send email with it's attachments to admin
        \Mail::send('emails/dwrEmail', ['project_name' => \Input::get('project_name'), 'count'=> $filecount, 'size' => $folderSize, 'types' => $filetypes,'uploadsDir' => $uploadLink, 'company' => $this->sub], function ($message){
            $message->from('workrequest@exergysolutions.com');
            if(strpos(\Request::ip(),"108.181.50.175") !== false ){$message->to('jordan@aquanode.com');}
            //else{$message->to('info@exergysolutions.com')->bcc('eric@aquanode.com');}
            $message ->subject('Work Request for '.\Input::get('project_name').' at '.\Input::get('project_site'));  
            
        });


        //send email to user
        \Mail::send('emails/dwrFormClient', ['name' => \Input::get('contact_name'), 'count'=> $filecount, 'size' => $folderSize, 'types' => $filetypes, 'company' => $this->sub], function ($message) use ($filename) {
            $message-> from('workrequest@exergysolutions.com');
            $message-> to(\Input::get('contact_email'));
            $message-> attach($filename);
            $message-> subject('Work Request for '.\Input::get('project_name').' at '.\Input::get('project_site'));  
        });

        //flash no errors and return original input 
        \Session::flash('error', false);
        \Request::flash();

        //clean up files from server and return
        if(file_exists($folderName)){
            $this->deleteDir($folderName);

            $uploads                = uploads::where('folder_name', $folderName)->first();
            $uploads->folder_name   = $zipname;
            $uploads->dwr_id        = $dwr->id;
            $uploads->save();

            }
        return \Redirect::to('/');
    }

    public function upload(){
     
        
        $folderName="uploads";
       
        //Empty out test Uploads folder
        if(file_exists ('uploads')){$this->deleteDir('uploads');}
        if(!file_exists($folderName)){mkdir($folderName);}
        return view('upload');
    }

    /**
     *
     * Check if all the parts exist, and 
     * gather all the parts of the file together
     * @param string $temp_dir - the temporary directory holding all the parts of the file
     * @param string $folderName - location of the temporary directory
     * @param string $fileName - the original file name
     * @param string $chunkSize - each chunk size (in bytes)
     * @param string $totalSize - original file size (in bytes)
     */
    function createFileFromChunks($temp_dir, $fileName, $chunkSize, $totalSize,$total_files, $upload_id) {

        //get upload temp file if not existing make one
        $folderName= uploads::where('upload_id', $upload_id)->pluck('folder_name');
        if(is_null($folderName)){
            $folderName=$this->sessionDir($upload_id);
        }
        
        if(!is_dir($folderName)&&!is_file($folderName)){mkdir($folderName);}

        
        // count all the parts of this file
        $total_files_on_server_size = 0;
        $temp_total = 0;
        foreach(scandir($temp_dir) as $file) {
            $temp_total = $total_files_on_server_size;
            $tempfilesize = filesize($temp_dir.'/'.$file);
            $total_files_on_server_size = $temp_total + $tempfilesize;
        }
        // check that all the parts are present
        // If the Size of all the chunks on the server is equal to the size of the file uploaded.
        if ($total_files_on_server_size >= $totalSize) {
        // create the final destination file 
            if (($fp = fopen($folderName."/".$fileName, 'w')) !== false) {
                for ($i=1; $i<=$total_files; $i++) {
                    fwrite($fp, file_get_contents($temp_dir.'/'.$fileName.'.part'.$i));
                }
                fclose($fp);
                $this->deleteDir($temp_dir);

            } else 
                return false;
            }

        }

    /** 
     * Upload method for large files accepting post chunk to temp directory then assembling them into a complete file
     * @param: none
     * @return: error 400 or success 200
     * @references: --http://www.resumablejs.com/
     *              --https://github.com/23/resumable.js/blob/master/samples/Backend%20on%20PHP.md
     */
    public function largeUpload($upload_id){
    
               // loop through files and move the chunks to a temporarily created directory
        if (!empty($_FILES)) foreach ($_FILES as $file) {
             //creating temporary file for the chunks if it doesn't exist
            $temp_dir= \Input::get('resumableIdentifier');
            
            if(is_null($temp_dir)){return \Response::json('error code: 8871', 500);}       
            if(!is_dir($temp_dir)){ mkdir($temp_dir);}

            // check the error status
            if ($file['error'] != 0) {
                //_log('error '.$file['error'].' in file '.$_POST['resumableFilename']);
                continue;
            }

            $dest_file = $temp_dir.'/'.\Input::get('resumableFilename').'.part'.\Input::get('resumableChunkNumber');

            // move the temporary file
            if (!move_uploaded_file($file['tmp_name'], $dest_file)) {
                return \Response::json('Error code: 9873', 400);
                //_log('Error saving (move_uploaded_file) chunk '.$_POST['resumableChunkNumber'].' for file '.$_POST['resumableFilename']);
            } else {
                // check if all the parts present, and create the final destination file
                $this->createFileFromChunks($temp_dir, \Input::get('resumableFilename'),\Input::get('resumableChunkSize'),\Input::get('resumableTotalSize'),\Input::get('resumableTotalChunks'), $upload_id);
            }
        }
        else{
            

            return \Response::json('Error code 3341: File Not Recieved', 500);
        }

    }


    /** 
     * Upload method for small files
     * @param: none
     * @return: error 400 or success 200
     */
    public function postUpload($upload_id){
        
        //get upload temp file if not existing make one
        $folderName= uploads::where('upload_id', $upload_id)->pluck('folder_name');
        if(is_null($folderName)){
            $folderName= $this->sessionDir($upload_id);
        }

        if(!is_dir($folderName)&&!is_file($folderName)){mkdir($folderName);}

        //get uploaded files
        $uploads = \Input::all();
        

        //move uploaded files to server
        $fileName = \Input::file('file')->getClientOriginalName(); 
        $upload_success = \Input::file('file')->move($folderName, $fileName); 

        //return status codes
        if ($upload_success) {            
            return \Response::json('success', 200);
        } else {
            return \Response::json('error', 400);
        }
    }

    /**
     * Method to recursively delete all files and delete the empty directory 
     * @param: Directory name
     * @return: function call to rmdir
    */
    public  function deleteDir($dir) { 
       $files = array_diff(scandir($dir), array('.','..')); 
        foreach ($files as $file) { 
          (is_dir("$dir/$file")) ? deleteDir("$dir/$file") : unlink("$dir/$file"); 
        } 
        return rmdir($dir); 
    }

    /**
     * Get directory size/ get file types
     * @param: dir name
     * @return: size of directory/ type of files
    */
    public  function getSize($dir) { 
        $totalSize=0;
        $files = array_diff(scandir($dir), array('.','..')); 
        foreach ($files as $file) { 
          (is_dir("$dir/$file")) ? getSize("$dir/$file") : $totalSize += fileSize("$dir/$file"); 
        } 
        $totalSize=$totalSize/1000000; 
        return ceil($totalSize); 
    }

     public function countfileTypes($dir){
        $extensions = [];
        $count =[];
        $files = array_diff(scandir($dir), array('.','..')); 
        foreach ($files as $file) {
            $parts = explode('.', $file);
            array_push($extensions, array_pop($parts));
        }
        $count = array_count_values($extensions);
        return $count;

    }

    /**
     * This function generates an upload ID
     *
     * @return (int) upload_id
     */
    public function getUploadId()
    {
        
        $recentUpload   = uploads::orderBy('upload_id', 'desc')->first();

        if(!is_null($recentUpload))
            return $recentUpload->upload_id+1;
        else
            return 1;
    }

    /**
     * Method creates a directory to store uploaded files 
     * @param: none
     * @return: none
    */
    public function sessionDir($upload_id){
        if($upload_id==0)
            $folderName             = 'uploads';
        else     
            $folderName             = rand(11111, 99999);
        $folderName                 = '_temp/'.$folderName;
        $uploadLoc                  = new uploads;
        $uploadLoc->upload_id       = $upload_id; 
        $uploadLoc->folder_name     = $folderName; 
        $uploadLoc->save();
        // \Session::put('fileName', $folderName);
        return $folderName;

    } 

    /**
     * Method creates a directory to store all files  
     * @param: none
     * @return: none
    */
    public function uploadDir(){
        //generate unique directory names
        do{
            $dirName = str_random(16);
            $dirName = '../uploads/'.$dirName;
        }while(is_dir($dirName));
        
        return $dirName;
    } 

    //User authentication
    public function login(){
        $name = \Input::get('username');
        $password = \Input::get('password');

        if ($this->loginId($name) != $this->sub['id'])
        {
            return \Response::json(['success' => FALSE, 'user' => FALSE, 'password' => FALSE]);
        }

        if (\Auth::attempt(['name' => $name, 'password' => $password])) {
            return \Response::json(['success' => TRUE]);
        }
        else if(User::where('name', '=', $name)->exists()){
            return \Response::json(['success' => FALSE, 'password' => FALSE, 'user' => TRUE]);
        }
        else {
            return \Response::json(['success' => FALSE, 'user' => FALSE, 'password' => FALSE]);
        }

    } 
   
   public function loginId($name)
   {
        $id = \DB::table('users')->where('name', $name)->pluck('company_id');
        return $id;
   }

    public function logDownload( $filename){
        

        // Check if file exists in app/storage/file folder
        $filename=$filename.".zip";
        $file_path =  "../uploads/".$filename;
        if (file_exists($file_path))
        {
            //log downloader's IP
            $userIp= \Request::ip();
            $dwr_file = dwr_files::create(
                ['download_ip'          => $userIp,
                'file_name'          => $filename]); 

            // Send Download
            return \Response::download($file_path, $filename, [
                'Content-Length: '. filesize($file_path)

            ]);
        }
        else
        {
            // Error
            exit('Requested file does not exist on our server!');
        }
    }



}