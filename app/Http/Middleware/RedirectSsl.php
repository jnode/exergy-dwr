<?php namespace App\Http\Middleware;

use Closure;


class RedirectSsl {


	public function handle($request, Closure $next)
    	{
        // Perform action
    	// remove www. prefix if it is set
		// if (strpos(\Request::url(), 'www.') !== false)
		// 	return \Redirect::to(str_replace('www.', '', str_replace('http://', 'https://', \Request::url())));

		// if (!\Request::secure())
		// 	return \Redirect::secure(\Request::path());
		
        return $next($request);
    }
	
		
}