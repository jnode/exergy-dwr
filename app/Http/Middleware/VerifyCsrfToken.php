<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

	//routes to be have exception from CSRF checks
	private $exceptions = ['uploadLargeFile', 'upload', 'der'];

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)

	{
		 //Skip check for specified routes 
	    foreach($this->exceptions as $route) {

	      if (strpos($request, $route) !== false) {
	        return $next($request);
	      }

	    }

		return parent::handle($request, $next);
	}

}
