<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class dwr extends Model
{ 	
	protected $table = 'dwr';
	protected $dates = ['deleted_at'];
    protected $fillable = [
        'company_id',
        'project_site',
    	'project_number',
        'project_name',
        'contact_name',
        'contact_last_name',
        'contact_title',
        'contact_email',
        'contact_phone',
        'completion_date',
        'alt_contact_name',
        'alt_contact_last_name',
        'alt_contact_phone',
        'alt_contact_email',
        'form_comments',
        'job_instructions',
        'native_drawing',
        'native_drawing_comment',
        'priority',
        'priority_comment',
        'hourly',
        'rate_comment',
    ];
}
