<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class uploads extends Model
{ 	
	protected $table = 'uploaded_files';
	protected $dates = ['deleted_at'];
    protected $fillable = [
        'upload_id',
        'dwr_id',
        'folder_name'    
    ];
}
