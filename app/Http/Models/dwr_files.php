<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class dwr_files extends Model
{
    protected $table = 'dwr_files';
    protected $dates = ['deleted_at'];
    protected $fillable = ['file_name', 'download_ip'];
   
}
