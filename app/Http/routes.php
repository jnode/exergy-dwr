<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|


Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
*/
Route::get('/', 							'dwrController@getDwrForm' );
Route::post('der', 							'dwrController@postDwrForm' );

Route::get('upload', 						'dwrController@upload' );
Route::post('upload/{upload_id}', 			'dwrController@postUpload' );
Route::post('uploadLargeFile/{upload_id}', 	'dwrController@largeUpload' );
Route::post('upload/delete', 				'dwrController@deleteUpload' );

Route::post('/login', 						'dwrController@login' );
// Download Route
Route::get('download/{filename}', 			'dwrController@logDownload' );