<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDwrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dwr', function (Blueprint $table) {
            $table->increments('id');
            $table->string('project_entity');
            $table->string('project_site');
            $table->string('project_name');
            $table->date('date');
            $table->date('request_date');
            $table->string('contact_name');
            $table->string('contact_title');
            $table->string('contact_email');
            $table->string('contact_phone');
            $table->string('mobile_phone');
            $table->text('job_instructions');
            $table->string('drawing_list');
            $table->boolean('alt_contact');
            $table->text('alt_contact_comment');
            $table->boolean('native_drawing');
            $table->text('native_drawing_comment');
            $table->text('new_drawing_comment');
            $table->boolean('priority');
            $table->text('priority_comment');
            $table->boolean('estimate');
            $table->text('estimate_comment');
            $table->boolean('scope');
            $table->text('scope_comment');
            $table->boolean('meeting');
            $table->text('meeting_comment');
            $table->integer('order_number');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dwr');
    }
}
