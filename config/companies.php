<?php

return [

	'companies' => [
			'dwr'	=> [
				'name'			=> 'Company',
				'id'			=> 0,
				'logo'			=> '',
				'sub'			=> 'dwr',
			],
			'enmax' 	=> [
				'name' 			=> 'Enmax',
				'id'			=> 1,
				'logo'			=> '/img/enmax_logo.png',
				'sub'			=> 'enmax',
			],
			'pmc'		=> [
				'name'			=> 'Plains Midstream Canada',
				'id'			=> 2,
				'logo'			=> '/img/pmc_logo.png',
				'sub'			=> 'pmc',
			],
			'spectra'	=> [
				'name'			=> 'Spectra Energy',
				'id'			=> 3,
				'logo'			=> '/img/spectra_logo.png',
				'sub'			=> 'spectra',
			],
			'suncor'	=> [
				'name'			=> 'Suncor Energy',
				'id'			=> 4,
				'logo'			=> '/img/suncor_logo.png',
				'sub'			=> 'suncor',
			],
	],

];

?>