<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>File Upload</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="/js/dropzone.js?343"></script>
	<script src="/js/resumable.js?343"></script>
	<script src="/js/jquery-dropzone.js?343"></script>
	<script src="/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="/css/dropzone.css">
	<link rel="stylesheet" href="/css/upload.css">
	<link rel="stylesheet" type="text/css" href="/css/app.css">

</head>
<body class=" dz-drag-hover">
	<input type="hidden" id ="token" name="_token" value="{!! csrf_token() !!}" />
	 <div id="actions" class="row">
		
      <div class="margin-bottom-10 col-lg-7">
        <!-- The fileinput-button span is used to style the file input field as button -->
        <span class="btn btn-primary fileinput-button dz-clickable" data-placement="bottom" data-toggle="tooltip" title="Please note only 1GB in total can be stored, we recommend a compressed .zip file for uploads larger than 50MB.">
            <i class="glyphicon glyphicon-plus"></i>
            <span>Add files...</span>
        </span>
        <button type="submit" class="btn btn-success start" data-placement="bottom" data-toggle="tooltip" title="Please note only 1GB in total can be stored, we recommend a compressed .zip file for uploads larger than 50MB.">
            <i class="glyphicon glyphicon-upload"></i>
            <span>Start upload</span>
        </button>
        <button onClick = "help()" class="btn btn-info " >
            <i class="glyphicon glyphicon-question-sign"></i>
            <span>Help</span>
        </button>
        <button id="clear-dropzone" class="btn btn-warning " >
            <i class="glyphicon glyphicon-paste"></i>
            <span>Clear Uploads</span>
        </button>
      </div>

      <div class="col-lg-5">
        <!-- The global file processing state -->
        <span class="hidden fileupload-process">
          <div style="opacity: 0;" id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress=""></div>
          </div>
        </span>
      </div>

    </div>
    <span id="help-message" class="hidden">To queue files for upload, drag files here or click the add files button. When all files are queued click start upload to submit them. For uploads larger than 50MB, please upload a compressed or zip version of them. Please note that this form can only store 1GB worth of files.</span>
    <hr class="fileDivider">
	<div class="box">
	
		<div  class="table table-striped" class="files" id="previews">

		  <div  id="template" class="margin-bottom-10 file-row">
		    <!-- This is used as the file preview template -->
		     <div class="fileButtons">
			     <button  type="submit"class="btn btn-primary start fileinput-button dz-clickable" >
			          <i class="glyphicon glyphicon-upload"></i>
			      </button>
			      <button data-dz-remove class="btn btn-danger delete" onClick="window.parent.iframeReset()">
			        <i class="glyphicon glyphicon-trash"></i>
			      </button>
		    </div>
		    <div id="thumb-div" class="fileIcon">
		        <span class="preview"><img data-dz-thumbnail style ="height: 25px; width: 25px;" /></span>
		    </div>
		    <div class="serverError">
		        <p class="name" data-dz-name></p>
		        <strong class="error text-danger" data-dz-errormessage></strong>
		    </div>
		    <div class="fileProgress pull-right">
		        <p class="size" data-dz-size></p>
		        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
		          <div id="current-progress" class="progress-bar progress-bar-success progress-bar-incomplete" style="width:0%; backgroundColor='#337ab7'" data-dz-uploadprogress> </div>
		        </div>
		    </div>
		   	<hr class="fileDivider">
		  </div>

		</div>
		
	</div>	
		<!-- HTML heavily inspired by http://blueimp.github.io/jQuery-File-Upload/ -->

</body>


</html>
