<body>
 Dear {!!$name!!},
<br>
<br>

Thank you for your estimate request. 
@if($count>0)
	Exergy Solutions will generate an estimate for the {!!$count!!} file(s) of size {!!$size!!}MBs that you have submitted as soon as possible (usually within 48 hours).  
	<br><br>
	Upload Details:
	<br>
	@foreach ($types as $ext => $count)
		{!!$count!!} file(s) of type {!!$ext!!}
		<br>
	@endforeach
@endif 
<br>
We will be contacting you for your approval and PO number along with your job number. If you have any additional questions or concerns, please feel free to contact us at info@exergysolutions.com. We have included a copy of the request for your reference and we look forward to working with you! 


<br>
<br>
Regards,
<br>
Exergy Solutions

</body>