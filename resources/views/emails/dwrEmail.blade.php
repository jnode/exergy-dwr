<body>
	Hello, 
	<br><br>
    This an autogenrated message from {!! $company['sub'] !!}.exergysolutions.com. <br>
    Within this <a href="https://{!!$company['sub']!!}.exergysolutions.com/download/{!!$uploadsDir!!}">download link</a> is the pdf version of the Drafting Estimate Request form 
	@if($count>0)
		and the {!!$count!!} file(s) with a total size of {!!$size!!}MBs 
		<br><br>
		Upload Details:
		<br>
		@foreach ($types as $ext => $count)
			{!!$count!!} file(s) of type {!!$ext!!}
			<br>
		@endforeach
	@endif
	that was submitted for the {!!$project_name!!} project.
	<br><br>
	If your email does not support dowload link then paste the following url: https://{!! $company['sub'] !!}.exergysolutions.com/download/{!!$uploadsDir!!} into your browser.
	<br><br>
	Have a good day.
</body>