
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="/css/app.css">
        <link rel="stylesheet" type="text/css" href="/css/style.css">
        <link rel="stylesheet" type="text/css" href="/css/dropzone.css">
        <link rel="stylesheet" href="/css/remodal.css">
        <link rel="stylesheet" href="/css/bootstrap-datepicker.min.css">
        <link rel="icon" href="img/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="/css/remodal-default-theme.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-datepicker.min.js"></script>
        <script src="/js/remodal.min.js"></script>
        <script src="/js/login.js"></script>
        <script>
            var error = JSON.parse("{{ json_encode($error) }}");
        </script>
        <script src="/js/dwr.js"></script>
        <script src="/js/dropzone.js?343"></script>
        <title>@yield('title')</title>

    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>