@extends('pdf.layouts.base')

@section('title', 'Drafting Work Request')

@section('content')	

    
	<!--Section 0: Header Area-->
	<table >
	  <tr>
	    <td colspan="1"><img class="logo" src=<?php echo URL::to('/img/exergy_logo.jpg');?> alt="Exergy Main Logo"></td>
	    <td colspan="4"><h1>Drafting Work Request</h1></td>
	    @if($company['id'] > 0)		
	    	<td colspan="1" align="right"><img class="logo" src= <?php echo URL::to($company['logo']);?> alt="Enmax Main Logo"></td>
	    @endif
	  </tr>
	 </table>

	<!---->

	<!--Section 1.0: Type of Estimate -->
	<h2>Type of Estimate</h2>
	@if($hourly)
		<div class="section shade"  >
			Hourly Rate - <?php if(!empty( $rate_comment)){echo  $rate_comment;} else{echo "No comments";}?>
		</div>
	@else
		<div class="section shade"  >
			Lump Sum - <?php if(!empty( $rate_comment)){echo  $rate_comment;} else{echo "No comments";}?>
		</div>
	@endif
	<!--Section 1.1: Project Fields and Contact fields -->
	
	<div class="section">
	     <!--Project Fields-->
     	<h2 class="">Project</h2>
		<table class="table_fields">
			<tr>
				<td colspan="1">MOC # and Name: </td>
				<td colspan="4" class="data-field"><?php echo $project_name ?></td> 
			</tr>
			<tr>
				<td colspan="1">Site: </td>
				<td  colspan="4" class="data-field"><?php echo $project_site ?></td> 
			</tr>
			
			<tr>
				<td colspan="1">Date: </td>
				<td colspan="4" class="data-field"><?php echo $date?></td> 
			</tr>
			<tr>
				<td colspan="1">Completion Date: </td>
				<td colspan="4" class="data-field"><?php echo $completion_date?></td> 
			</tr>

		</table>
	</div>

			
	<div class="section">
	     <!--Project Fields-->
     	<h2 class=""><?=$company['name']?> Contact</h2>
		<table class="table_fields">
			<tr>
				<td colspan="1">First Name: </td>
				<td colspan="4" class="data-field" ><?php echo $contact_name ?></td> 
			</tr>
			<tr>
				<td colspan="1">Last Name: </td>
				<td colspan="4" class="data-field" ><?php echo $contact_last_name ?></td> 
			</tr>
			<tr>
				<td colspan="1">Title: 	</td>
				<td colspan="4" class="data-field"><?php echo $contact_title ?></td> 
			</tr>
			<tr>
				<td colspan="1">Email: </td>
				<td colspan="4" class="data-field"><?php echo $contact_email ?></td> 
			</tr>
			<tr>
				<td colspan="1">Phone: </td>
				<td colspan="4" class="data-field"><?php echo $contact_phone?></td> 
			</tr>
		</table>
	</div>	

	@if(!empty($alt_contact_name)||!empty($alt_contact_email)||!empty($alt_contact_phone))
		<div class="section">
		     <!--Project Fields-->
	     	<h2 class="">Alternative Contact</h2>
			<table class="table_fields">
				<tr>
					<td colspan="1">First Name:</td>
					<td colspan="4" class="data-field" ><?php if(!empty($alt_contact_name)){echo $alt_contact_name;}else{echo "Unavailable";} ?></td> 
				</tr>
				<tr>
					<td colspan="1">Last Name:</td>
					<td colspan="4" class="data-field" ><?php if(!empty($alt_contact_last_name)){echo $alt_contact_last_name;}else{echo "Unavailable";} ?></td> 
				</tr>
				<tr>
					<td colspan="1">Email:</td>
					<td colspan="4" class="data-field"><?php if(!empty($alt_contact_email)){echo $alt_contact_email;}else{echo "Unavailable";} ?></td> 
				</tr>
				<tr>
					<td colspan="1">Phone:</td>
					<td colspan="4" class="data-field"><?php if(!empty($alt_contact_phone)){echo $alt_contact_phone;}else{echo "Unavailable";} ?></td> 
				</tr>
			</table>
		</div>	
	@endif
	<!--End of section 1-->

	<br>

	<!--Section 1.2: Comments-->
	<h2>Comments</h2>
	<div class="section job shade"  >
		<?php if(!empty($form_comments)){echo $form_comments;} else{echo "No comments";}?>
	</div>
	<!--Section 2: Job Description-->
	<h2>Job Instructions</h2>
	<div class="section job shade"  >
		<?php echo $job_instructions ?>
	</div>
	
	<!--Section 3: Checklist-->
	
	<div class="checklist"  >
		<h2>Checklist</h2>
		<table >
			<thead>
				<tr class="bck-drkgrey">
					<th>#</th>
					<th>Item to Consider</th>
					<th>Response</th>
					<th>Comments</th>
				</tr>
			</thead>
			<tr class="bck-grey">
				<td>1</td>
				<td>Have all native CAD drawing files been provided?</td>
				<td><?php if($native_drawing){ echo "Yes";}else{echo "No";} ?></td>
				<td><?php echo $native_drawing_comment ?></td>
			</tr>
			<tr class="bck-white">
				<td>2</td>
				<td>Are there any high priority drawings?</td>
				<td><?php if($priority){ echo "Yes";}else{echo "No";} ?></td>
				<td><?php echo $priority_comment; ?></td>
			</tr>
		</table>
		
	</div>
	


@stop
