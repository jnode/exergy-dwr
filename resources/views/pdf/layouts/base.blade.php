<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href= <?php echo URL::to('/css/pdfStyle.css') ?>>
        <title>@yield('title')</title>

    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>