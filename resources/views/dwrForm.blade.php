<?php $customFunctions = new \App\Library\customFunctions; ?>

@extends('layouts.master')

@section('title', 'Drafting Work Request')

@section('content')	

	{!!Form::open(array('url' => '/der', 'id'=>'dwrForm'));!!}
    <div class="remodal-bg">
		<!--Section 0: Header Area-->

		<div class="row margin-top-50">
			<div class="mobile-logo row col-sm-12">	
					<img class="exergylogo_mobile" src="/img/exergy_logo.png" alt="Exergy Main Logo">
					<img class="enmaxlogo_mobile pull-right" src="/img/enmax_logo.png" alt="Enmax Main Logo">
			</div>
			<div class="row">
				<div class="col-sm-2">	
					<img class="exergylogo" src="/img/exergy_logo.png" alt="Exergy Main Logo">
				</div>
				<div class="col-sm-8">
					<h1>Drafting Work Request</h1> 
				</div>
				<div class="col-sm-2">	
					<!-- <img class="enmaxlogo pull-right" src="/img/enmax_logo.png" alt="Enmax Main Logo"> -->
					@if($company['id'] > 0)	
						<img class="enmaxlogo pull-right" src="{{ $company['logo'] }}" alt="{!! $company['name'] !!} Main Logo">
					@endif
				</div>
			</div>
			<!--Messages for the user-->
			<div class="alert alert-info margin-top-15 ">
	            <span class="glyphicon glyphicon-info-sign"></span>
	            <strong>
	            Disclaimer: The following estimate form is a request for quotation purposes only. Exergy Solutions will generate an estimate within 48 hours. 
	        	</strong>
	         </div>	
			<div id="success_message" class="alert alert-success hidden">
	            <span class="glyphicon glyphicon-ok"></span><strong> Your form has been successfully submitted to Exergy Solutions.
				 <?php 
				 if($count>0){ echo("A total of ".$count." file(s) have been uploaded with a total size of ".$size." MBs"); } ?>
	        	</strong>
	         </div>	

			@if (count($errors) > 0) 
					<div class="alert alert-danger">
	                    <span class="glyphicon glyphicon-remove"></span><strong> There was at least one error found, Please check the marked input fields and upload your files again.</strong>
	                </div>	
				<div class=" error col-lg-3 well well-sm required-field">
			@else
				<div class=" col-lg-3 well well-sm required-field">
			@endif
				
		
				<strong><span class=" glyphicon glyphicon-asterisk"></span> These fields are required</strong></div>
		</div>
		
		<!--Section 1.0: Estimate type field -->
		<div class="row section">
			<div class="form-group">
					<h2>Choose Type of Estimate</h2>
					<div class="input-group ">
						<span class="{!!$customFunctions->checkClass('input-group-addon', 'error', $errors->has('hourly'));!!}">
						<span class="glyphicon glyphicon-asterisk"></span></span>
						<span class="{!!$customFunctions->checkClass('input-group-addon no-border-right', 'error', $errors->has('hourly'));!!}">
							<div>
								<div class="pull-right">
									{!!Form::radio('hourly', '1');!!}
									{!! Form::label('Hourly Rate - Budgetary Estimate'); !!}
								</div>
								<div class="pull-left">
									{!!Form::radio('hourly','0');!!}
									{!! Form::label('Lump Sum - Fixed Budget'); !!}
								</div>
							</div>
						</span>
						{!! Form::text('rate_comment','', array('placeholder'=>'Comments...', 'style="height:54px"','class' =>'border-right-delete form-control')); !!}
						
						<span class="{!!$customFunctions->checkClass('input-group-addon asterisk', 'error', $errors->has('hourly'));!!}"></span>
					</div>
					<span class="{!!$customFunctions->checkClass('help', 'hidden', !$errors->has('hourly'));!!}">Please make a selection</span>
				</div>
		</div>

		<!--Section 1.1: Project Fields -->
		<div class="row section">
		     <!--Project Fields-->	     	
		     	<h2>Project</h2>
		     	<!-- Select box for Enmax -->
		     	@if($company['id'] == 1)
					<div class="form-group col-md-6">	
						{!! Form::label('Site'); !!}
						<div class="input-group">	
							{!! Form::select('project_site', array('Shepard Energy Centre'=>'Shepard Energy Centre', 'Calgary Energy Centre'=>'Calgary Energy Centre', 'Crossfield Energy Centre'=>'Crossfield Energy Centre', 'Cavalier Energy Centre'=>'Cavalier Energy Centre', 'Taber Wind Farm'=>'Taber Wind Farm', 'Kettles Hill Wind Farm'=>'Kettles Hill Wind Farm', 'District Energy 9th Avenue'=>'District Energy 9th Avenue'),NULL, array('style'=>'-webkit-appearance: none;','class' =>'border-right-delete form-control')); !!}
							<span class="{!!$customFunctions->checkClass('input-group-addon asterisk', 'error', (count($errors) > 0 && $errors->has('project_site')));!!}">
							<span class="glyphicon glyphicon-asterisk"></span></span>
						</div>
						<span class="{!!$customFunctions->checkClass('help', 'hidden', !(count($errors) > 0 && $errors->has('project_site')));!!}">Please enter a valid site name</span>
					</div>
				@else
					<div class="form-group col-md-6">	
						{!! Form::label('Site'); !!}
						<div class="input-group">	
							{!! Form::text('project_site' , '', array('placeholder'=>'Enter Project Name', 'class' =>'border-right-delete form-control')); !!}
							<span class="{!!$customFunctions->checkClass('input-group-addon asterisk', 'error', (count($errors) > 0 && $errors->has('project_site')));!!}">
							<span class="glyphicon glyphicon-asterisk"></span></span>
						</div>
						<span class="{!!$customFunctions->checkClass('help', 'hidden', !(count($errors) > 0 && $errors->has('project_site')));!!}">Please enter a valid site name</span>
					</div>
				@endif

				<div class="form-group col-md-6">	
					{!! Form::label('Required Completion Date'); !!}
					<div class="input-group date">
						<span class="{!!$customFunctions->checkClass('input-group-addon', 'error', (count($errors) > 0 && $errors->has('completion_date')));!!}">
						<span class="glyphicon glyphicon-calendar"></span></span>
						{!! Form::text('completion_date','', array('placeholder'=> 'yyyy-mm-dd', 'id'=>'completionDate','class' =>'border-right-delete form-control')); !!}
						<span class="{!!$customFunctions->checkClass('input-group-addon asterisk', 'error', (count($errors) > 0 && $errors->has('completion_date')));!!}">
						<span class="glyphicon glyphicon-asterisk"></span></span>
					</div>
					<span class="{!!$customFunctions->checkClass('help', 'hidden', !(count($errors) > 0 && $errors->has('completion_date')));!!}">Please enter a valid date</span>
				</div>		
				<div class="form-group col-md-6">	
					{!! Form::label('MOC # and Project name'); !!}
					<div class="input-group">	
						{!! Form::text('project_name' , '', array('placeholder'=>'Enter Project Name', 'class' =>'border-right-delete form-control')); !!}
						<span class="{!!$customFunctions->checkClass('input-group-addon asterisk', 'error', (count($errors) > 0 && $errors->has('project_name')));!!}">
						<span class="glyphicon glyphicon-asterisk"></span></span>
					</div>
					<span class="{!!$customFunctions->checkClass('help', 'hidden', !(count($errors) > 0 && $errors->has('project_name')));!!}">Please enter a valid project name</span>
				</div>	
		</div>	
		<!--End of section 1-->

		<!--Section 1.5: Enmax and Alternative Contact-->
		<div class="row section">
			
			<!--Enmax Contact Fields-->
		     <div class="col-lg-6">
		     	<h2>{!! $company['name'] !!} Contact</h2>
				
				<div class="form-group col-sm-6 mini-input-left">	
					{!! Form::label('First Name');!!}
					<div class="input-group">
						{!! Form::text('contact_name','', array('placeholder'=>'Enter First Name', 'class' =>'border-right-delete form-control')); !!}
						<span class="{!!$customFunctions->checkClass("input-group-addon asterisk", "error", (count($errors) > 0 && $errors->has('contact_name')));!!}">
						<span class="glyphicon glyphicon-asterisk"></span></span>
					</div>
					<span class="{!!$customFunctions->checkClass("help", "hidden", !(count($errors) > 0 && $errors->has('contact_name')));!!}">Please enter a valid name</span>
				</div>

				<div class="form-group col-sm-6 mini-input-right">	
					{!! Form::label('Last Name');!!}
					<div class="input-group">
						{!! Form::text('contact_last_name','', array('placeholder'=>'Enter Last Name', 'class' =>'border-right-delete form-control')); !!}
						<span class="{!!$customFunctions->checkClass("input-group-addon asterisk", "error", (count($errors) > 0 && $errors->has('contact_name')));!!}">
						<span class="glyphicon glyphicon-asterisk"></span></span>
					</div>
					<span class="{!!$customFunctions->checkClass("help", "hidden", !(count($errors) > 0 && $errors->has('contact_name')));!!}">Please enter a valid name</span>
				</div>

				<div class="form-group">	
					{!! Form::label('Title'); !!}
					<div class="input-group">
						{!! Form::text('contact_title','', array('placeholder'=>'Enter Title', 'class' =>'border-right-delete form-control')); !!}
						<span class="{!!$customFunctions->checkClass("input-group-addon asterisk", "error", (count($errors) > 0 && $errors->has('contact_title')));!!}">		
						<span class="glyphicon glyphicon-asterisk"></span></span>
					</div>
					<span class="{!!$customFunctions->checkClass("help", "hidden", !(count($errors) > 0 && $errors->has('contact_title')));!!}">Please enter a valid contact title</span>
				</div>			
				
				<div class="form-group">	
					{!! Form::label('Email'); !!}
					<div class="input-group">
						<span class="{!!$customFunctions->checkClass("input-group-addon", "error", (count($errors) > 0 && $errors->has('contact_email')));!!}">
						<span class="glyphicon glyphicon-envelope"></span></span>
						{!! Form::text('contact_email','', array('placeholder'=>'Enter Email Address', 'class' =>'border-right-delete form-control')); !!}
						<span class="{!!$customFunctions->checkClass("input-group-addon asterisk", "error", (count($errors) > 0 && $errors->has('contact_email')));!!}">
						<span class="glyphicon glyphicon-asterisk"></span></span>
					</div>
					<span class="{!!$customFunctions->checkClass("help", "hidden", !(count($errors) > 0 && $errors->has('contact_email')));!!}">Please enter a valid email address</span>	
				</div>			
				
				<div class="form-group">	
					{!! Form::label('Phone'); !!}
					<div class="input-group">
						<span class="{!!$customFunctions->checkClass("input-group-addon", "error", (count($errors) > 0 && $errors->has('contact_phone')));!!}">
						<span class="glyphicon glyphicon-phone"></span></span>

						{!! Form::text('contact_phone','', array('placeholder'=>'Enter Phone Number', 'class' =>'border-right-delete form-control')); !!}
						<span class="{!!$customFunctions->checkClass("input-group-addon asterisk", "error", (count($errors) > 0 && $errors->has('contact_phone')));!!}">
						<span class="glyphicon glyphicon-asterisk"></span></span>
					</div>
					<span class="{!!$customFunctions->checkClass("help", "hidden", !(count($errors) > 0 && $errors->has('contact_phone')));!!}">Please enter a valid phone number</span>
				</div>			
			</div> 

		    <div class="col-lg-6">
		     	<h2>Alternative Contact</h2>
				<div class="form-group col-sm-6 mini-input-left">	
					{!! Form::label('First Name');!!}
					<div class="input-group ">
						{!! Form::text('alt_contact_name','', array('placeholder'=>'Enter First Name', 'class' =>'border-right-delete form-control')); !!}
						<span class="input-group-addon asterisk"><span class="glyphicon">
					</div>
				</div>
				<div class="form-group col-sm-6 mini-input-right">	
					{!! Form::label('Last Name');!!}
					<div class="input-group">
						{!! Form::text('alt_contact_last_name','', array('placeholder'=>'Enter Last Name', 'class' =>'border-right-delete form-control')); !!}
						<span class="input-group-addon asterisk"><span class="glyphicon">
					</div>
				</div>		
				
				<div class="form-group">	
					{!! Form::label('Email'); !!}
					<div class="input-group">
						<span class="{!!$customFunctions->checkClass("input-group-addon", "error", (count($errors) > 0 && $errors->has('alt_contact_email')));!!}">
						<span class="glyphicon glyphicon-envelope"></span></span>
						{!! Form::text('alt_contact_email','', array('placeholder'=>'Enter Email Address', 'class' =>'border-right-delete form-control')); !!}
						<span class="{!!$customFunctions->checkClass("input-group-addon asterisk", "error", (count($errors) > 0 && $errors->has('alt_contact_email')));!!}">
						<span class="glyphicon "></span></span>
					</div>
					<span class="{!!$customFunctions->checkClass("help", "hidden", !(count($errors) > 0 && $errors->has('alt_contact_email')));!!}">Please enter a valid email address</span>
				</div>			
				
				<div class="form-group">	
					{!! Form::label('Phone'); !!}
					<div class="input-group">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-phone"></span></span>
						{!! Form::text('alt_contact_phone','', array('placeholder'=>'Enter Phone Number', 'class' =>'border-right-delete form-control')); !!}
						<span class="input-group-addon asterisk">
						<span class="glyphicon"></span></span>
					</div>
				</div>
				<div class="form-group">
					{!! Form::label('Comments'); !!}
					<div class="input-group">
						{!!Form::textarea('form_comments','', array( 'class' => 'border-right-delete form-control', 'rows'=>"2"));!!}
						<span class="input-group-addon asterisk">
						<span class="glyphicon "></span></span>
					</div>	
				</div>		
			</div> 
		</div>	
		<!--End of section 1.5-->

		<!--Section 2: Job Instructions-->
		<div class="row section">
			<div class="col-lg-12">
				<div class="form-group">
					{!! Form::label('Job Instructions'); !!} 
					<div class="alert alert-info margin-top-15 ">
			            <span class="glyphicon glyphicon-info-sign"></span>
			            <strong>
			            You may provide a brief description of the job you want completed, any site/project information, or standards and style requests. 
			        	</strong>
			        </div>	   
					<div class="input-group">
						{!!Form::textarea('job_instructions','', array( 'class' => 'border-right-delete form-control', 'rows'=>"5"));!!}
						<span class="{!!$customFunctions->checkClass('input-group-addon asterisk', 'error', (count($errors) > 0 && $errors->has('job_instructions')));!!}">
						<span class="glyphicon glyphicon-asterisk"></span></span>
					</div>
				</div>
			</div>
			<span class="{!!$customFunctions->checkClass("help", "hidden", !(count($errors) > 0 && $errors->has('job_instructions')));!!}">Please enter valid instructions</span>
		</div>
		<!--End of Section 2-->
		<!--Section 3: File Upload-->
		<div id="fileUploadContainer"class="row section">
			{!! Form::label('Secure File Upload'); !!} 
			<img src="img/comodo_secure_76x26_transp.png" alt="comdodo security icon" class="pull-right">
			@if($count>0)
				<div id="" class="alert alert-success">
		            <strong> A total of <?php echo($count); ?> file(s) have been uploaded with a total size of <?php echo ($size);?> MBs</strong>
		        </div>	
			@endif
			<br>
			<iframe id="upload-frame" src="/upload" frameborder="0" onload="iframeLoaded(); iframeReset();"></iframe>
		</div>
		<!--End of Setion 3-->
		<!--Section 4: Checklist-->
		<div class="row section">
			<div class="col-lg-12">

				<div class="form-group">
					{!! Form::label('Have All Native CAD Drawing Files Been Provided? (If No, Please Comment)'); !!}
					<div class="input-group">
						<span class="{!!$customFunctions->checkClass('input-group-addon ', 'error', ($errors->has('native_drawing')||$errors->has('native_drawing_comment')));!!}"><span class="glyphicon glyphicon-asterisk"></span>
						</span>
						<span class="{!!$customFunctions->checkClass('input-group-addon no-border-right', 'error', ($errors->has('native_drawing')||$errors->has('native_drawing_comment')));!!}">
						{!! Form::label('Yes'); !!}
						{!!Form::radio('native_drawing','1');!!}
						{!! Form::label('No'); !!}	
						{!!Form::radio('native_drawing','0');!!}
						</span>
						{!! Form::text('native_drawing_comment','', array('placeholder'=>'Comments...', 'class' =>'border-right-delete form-control')); !!}
						<span class="{!!$customFunctions->checkClass('input-group-addon asterisk', 'error', ($errors->has('native_drawing')||$errors->has('native_drawing_comment')));!!}"></span>
					</div>
					<span class="{!!$customFunctions->checkClass('help', 'hidden', !($errors->has('native_drawing')||$errors->has('native_drawing_comment')));!!}">Please make a selection, if no please add a comment</span>
				</div>
				
				<input type="hidden" id ="token" name="_token" value="{!! csrf_token() !!}" /> <!--Shh, you never saw this-->
				<input type="hidden" id ="upload_id" name="upload_id" value="{!! $upload !!}" /> <!--Shh, you never saw this-->

				<div class="form-group">
					{!! Form::label('Are There Any High Priority Drawings That Require Urgent Attention? (If Yes, Please Comment)'); !!}
					<div class="input-group">
						<span class="{!!$customFunctions->checkClass('input-group-addon', 'error', $errors->has('priority'));!!}">
							<span class="glyphicon glyphicon-asterisk"></span>
						</span>
						<span class="{!!$customFunctions->checkClass('input-group-addon no-border-right', 'error', ($errors->has('priority')||$errors->has('priority_comment')));!!}">
						{!! Form::label('Yes'); !!}
						{!!Form::radio('priority', '1');!!}
						{!! Form::label('No'); !!}
						{!!Form::radio('priority','0');!!}
						</span>
						{!! Form::text('priority_comment','', array('placeholder'=>'Comments...', 'class' =>'border-right-delete form-control')); !!}
						<span class="{!!$customFunctions->checkClass('input-group-addon asterisk', 'error', $errors->has('priority'));!!}"></span>
					</div>
					<span class="{!!$customFunctions->checkClass('help', 'hidden', !$errors->has('priority'));!!}">Please make a selection, if yes please add a comment</span>
				</div>
		
			</div>
			
		</div>
		<!--End of Setion 4-->
		
		<!--Section 4.1: Workflow Diagram-->
		@if($company['id'] == 1)
		<div id="workflow-section" class="row section">
			{!! Form::label('Work Request Process'); !!}    
			<img name="workflow" src="/img/workflow.png" width="1138" height="162" border="0" id="workflow" usemap="#m_workflow" alt="" />
			<map name="m_workflow" id="m_workflow">
				<area id="step4"shape="poly" coords="801,4,1064,4,1123,80,1065,156,801,157,868,80" href="javascript:;" alt="" />
				<area id="step3"shape="poly" coords="537,4,793,5,859,80,792,157,537,157,605,80" href="javascript:;" alt="" />
				<area id="step2"shape="poly" coords="274,4,340,80,272,156,527,155,594,80,526,3" href="javascript:;" alt="" />
				<area id="step1"shape="poly" coords="9,4,264,4,331,81,263,156,11,157,60,81" href="javascript:;" alt="" />
			</map>
			<div class="step1 enmaxstep hidden">
				<p>
					Our simple Drafting Work Request system is a secure easy access portal to request work from Exergy Solutions.
				</p>
			</div>
			<div class="step2 exergystep hidden">
				<p>
					Our Lead CAD Designers will promptly review your DWR request and submit an estimate within 48 hours or less.
				</p>
			</div>
			<div class="step3 enmaxstep hidden">
				<p>
					Upon approval of estimate, Exergy Solutions will require a PO # and Project code (E-code, C-code or MOC) in order to start drafting work.
				</p>
			</div>
			<div class="step4 exergystep hidden">
				<p>
					Drafting work will be completed and submitted to client.
				</p>
			</div>
		</div>
		@endif
		<!-- End of Section 4.1-->


		<!--Section 5: Submit-->
		<div class="row padding-bottom-50 margin-top-50">
			<div class="col-lg-12">
				<!--Messages for the user-->
				<div id="upload-warning" class="alert alert-warning hidden">
		            <span class="glyphicon glyphicon-warning-sign"></span>
		            <strong>
		            There are some queued files that were not uploaded, please upload them or delete them from the queue before submitting the form.
		        	</strong>
		         </div>	
				{!!Form::submit('Submit For Estimate', array('id'=>'submitButton', 'class' => 'btn btn-success pull-left', 'data-toggle' => 'tooltip', 'title' => 'Please ensure all uploads are complete before submiting','onClick'=>'overlay()' ));!!}
				{!!Form::button('Clear Form', array('class' => 'btn btn-info pull-right', 'id' => 'reset' ));!!}
			</div>

			

			{!! Form::close(); !!}
		</div>
		<!--End of Setion 5-->
	</div>

	<img id="spinner" src="img/loading.gif" alt="Page is currently loading">
	<!--Login Overlay-->
	<div class="remodal login" id = "login-modal" data-remodal-id="login" data-remodal-options ="closeOnAnyClick: false, closeOnConfirm: false, closeOnOutsideClick: false, closeOnEscape: false, closeOnCancle: false">
		<h2>Secure Login</h2>	
		<hr>
		<h1>Welcome to Exergy Solutions</h1>
		<span>Please provide your username and password to access content</span>
		<hr>
		{!! Form::open(['id' =>'login-form','class' => 'uk-form']) !!}
			<span id="wrng-usrnme" class="invalid hidden">The credentials do not match our records.</span>
			<div class="input-group">
				<span id="user-form"class="input-group-addon ">
				<span class="glyphicon glyphicon-user"></span></span>
				{!! Form::text('username','', array('placeholder'=>'Username', 'class' =>'form-control')); !!}
			</div>
			<hr>
			<span id="wrng-psswd" class="invalid hidden">Incorrect Password</span>
			<div class="input-group">
				<span id="pass-form" class="input-group-addon ">
				<span class="glyphicon glyphicon-lock"></span></span>
				{!! Form::password('password', array( 'onClick'=>'resetForm()', 'class' =>'form-control')); !!}
			</div>
			<hr>
			<div>
				<a  id ="forgot_psswd" href="javascript:void(0)">Forgot your password?</a>
				<div id="forgot_help" class="alert alert-info hidden" role="alert">Please contact Exergy Solutions for your login information at info@exergysolutions.com or call 403-700-9333 for assistance</div>
			</div>
		{!!Form::close()!!}
		<div>
			<img src="img/comodo_secure_76x26_transp.png" alt="comdodo security icon" class="pull-left">
			<button id="login-button" class="btn btn-success pull-right" onclick="validateForm()">LOGIN</button>
		</div>
		<div class="uk-width-1-4">&nbsp;</div>
		
	</div>


	<!--End of Login Overlay
	<button id = "login-modal-btn" data-remodal-action="close" class="remodal-close"></button>-->

@stop
