checkLogin();


$(document).keypress(function(e) {
    if(e.which == 13) {
        validateForm();
    }
});

function checkLogin(){
  var logCheck = sessionStorage.loggedIn;
  if (logCheck == null || logCheck == false)
    {window.location.href = "/#login";}

}

function validateForm() {
    var user = document.forms["login-form"]["username"].value.trim();
    var pass = document.forms["login-form"]["password"].value.trim();
    var token = $('#token').val();
    
    //validate user name
    if (user == null || user == "") {
       $('#user-form').addClass("error");
    }
  
    //validate password
    if (pass == null || pass == ""){
       $('#pass-form').addClass("error");
    }
    user=user.toLowerCase();
    $.ajax({
        type: "POST",
        url : "/login",
        data: $('#login-form').serialize(),
        headers: {
         'X-CSRF-Token': token},
        success : function(data){
            
            if(data.success){ 
             //Successful login 
              $('#user-form').removeClass("error");
              $('#pass-form').removeClass("error"); 
              $('#wrng-psswd').addClass('hidden');
              $('#wrng-usrnme').addClass('hidden');
              try{
                sessionStorage.setItem("loggedIn", 1);
                }
              catch (e){
                console.log("Cannot access sessionStorage");
                alert("Unable to log into form in Private Browsing Mode");
              } 
              window.location.href = "/";
            }
            //unsuccessful login
            else{
              //console.log("Log in Unsuccessful"); 
              if(!data.password)
              {
                $('#pass-form').addClass("error");
                $('#wrng-psswd').removeClass('hidden');
                document.forms["login-form"]["password"].value="";
              }
              else{
                $('#pass-form').removeClass("error");
                $('#wrng-psswd').addClass('hidden');
              }
         
            if(!data.user)
              {
                $('#user-form').addClass("error");
                $('#wrng-usrnme').removeClass('hidden');
                $('#wrng-psswd').addClass('hidden');
              }
            else{
              $('#user-form').removeClass("error");
              $('#wrng-usrnme').addClass('hidden');
            }

            }
          }

        },"json");


    

    

}

