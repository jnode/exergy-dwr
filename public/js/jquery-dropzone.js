$( document ).ready(function() {
   $('[data-toggle="tooltip"]').tooltip();  
  // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
  var previewNode = document.querySelector("#template");
  previewNode.id = "";
  var previewTemplate = previewNode.parentNode.innerHTML;
  previewNode.parentNode.removeChild(previewNode);
  var token = $('#token').val();
  var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
    url: "/upload/0", // Set the url
    thumbnailWidth: 80,
    thumbnailHeight: 80,
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    autoQueue: false, // Make sure the files aren't queued until manually added
    previewsContainer: "#previews", // Define the container to display the previews
    clickable: ".fileinput-button",// Define the element that should be used as click trigger to select files.
    headers: {
         'X-CSRF-Token': token
    }
  });

  myDropzone.on("addedfile", function(file) {
    var uploadId = $("#upload_id", window.parent.document).val();
    this.options.url = "/upload/"+uploadId; // Set the url

    // Hookup the start button
    file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
  });

  // Update the total progress bar
  myDropzone.on("totaluploadprogress", function(progress) {
    var bar = document.querySelector("#total-progress .progress-bar");
    bar.style.width = progress + "%";
   /*
    if(progress === 100){
      console.log(progress);
      $("#current-progress").removeClass("progress-bar-incomplete");
    }*/
  });

  myDropzone.on("sending", function(file) {
    // Show the total progress bar when upload starts
    document.querySelector("#total-progress").style.opacity = "1";
    // And disable the start button
    file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
  });

  // Hide the total progress bar when nothing's uploading anymore
  myDropzone.on("queuecomplete", function(progress) {
    document.querySelector("#total-progress").style.opacity = "0";
  });

  // Setup the buttons for all transfers
  // The "add files" button doesn't need to be setup because the config
  // `clickable` has already been specified.
  document.querySelector("#actions .start").onclick = function() {
    myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
  };


  // Setup the observer for the clear button.
  document.querySelector("button#clear-dropzone").onclick = function() {
    myDropzone.removeAllFiles(true);
    window.parent.iframeReset();
  };

  //disabling submit button with files on queue
  if ($('#submitButton', parent.document).length) {
     myDropzone.on("addedfile", function (file) {
          //console.log("A file has been added");
        $('#upload-warning', parent.document).removeClass("hidden");
      });
     myDropzone.on("queuecomplete", function (file) {
          //console.log("All files have uploaded ");
        $('#upload-warning', parent.document).addClass("hidden");
      });
     myDropzone.on("reset", function (file) {
          //console.log("All files have uploaded ");
        $('#upload-warning', parent.document).addClass("hidden");
      });
    

  }

}); 

function help(){
    event.preventDefault();
    $('#help-message').toggleClass("hidden");
  }