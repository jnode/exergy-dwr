// A $( document ).ready() block.
$( document ).ready(function() {

 
  $('#completionDate').datepicker({
    format: 'yyyy-mm-dd'
  });
  
  $('[data-toggle="tooltip"]').tooltip();  
   if(error===false){
   		$('#success_message').removeClass('hidden');
      $('#reset').attr("disabled", true);
      $('#submitButton').attr("disabled", true);

   }

   overlay = function(){
      $(".remodal-overlay").fadeIn().css("display", "block");
      $("#spinner").fadeIn().css("display", "block");
      $("html, body").animate({ scrollTop: 0 }, "slow");

   }

  
  iframeLoaded=function() {
    var iFrameID = document.getElementById('upload-frame');
    if(iFrameID) { 
        iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
   }   
   
  } 
  
   iframeReset=function() {
    var iFrameID = document.getElementById('upload-frame');
    if(iFrameID) { 
        iFrameID.height = "";
   }   
   
  } 

  $('#fileUploadContainer').mouseover(function() {
    eventFire=setInterval(function(){ 
      try{

         if($('#upload-frame').height()<600) 
          {iframeLoaded();} 
        else{
          $('#upload-frame').height(600);
          clearInterval();}
        
        
        }
      catch(err) {clearInterval();}
    }, 50);
  }); 

  //auto logout the user after 90 minutes
  setTimeout(function(){ 
    try{
      sessionStorage.setItem("loggedIn", 0);
      window.location.replace("https://enmax.exergysolutions.com/#login");
    }
    catch (e){
      console.log("Cannot access sessionStorage");
    }

  }, 4500000);
 
 //Toggle forgot pass word help message
 $('#forgot_psswd').click(function(){ $('#forgot_help').toggleClass("hidden");})

 //workflow js

 //remove class after load to hide elements on page load
 $(".step1").removeClass("hidden");
 $(".step2").removeClass("hidden");
 $(".step3").removeClass("hidden");
 $(".step4").removeClass("hidden");

 //using js to hide
  $(".step1").hide();
  $(".step2").hide();
  $(".step3").hide();
  $(".step4").hide();

//adding hover effect
$( "#step1" ).mouseenter(function() {
  $( ".step1" ).slideDown();
});
$( "#step1" ).mouseleave(function() {
  $( ".step1" ).slideUp();
});

$( "#step2" ).mouseenter(function() {
  $( ".step2" ).slideDown();
});
$( "#step2" ).mouseleave(function() {
  $( ".step2" ).slideUp();
});

$( "#step3" ).mouseenter(function() {
  $( ".step3" ).slideDown();
});
$( "#step3" ).mouseleave(function() {
  $( ".step3" ).slideUp();
});

$( "#step4" ).mouseenter(function() {
  $( ".step4" ).slideDown();
});
$( "#step4" ).mouseleave(function() {
  $( ".step4" ).slideUp();
});


});

$(function() {
    $('#reset').click(function() {
    	//reset iframe
    	document.getElementById('upload-frame').contentDocument.location.reload(true);
        //reset form
         $(':input')
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .removeAttr('checked')
            .removeAttr('selected');
    });
 

});

